package com.example.dawyson.contadodepassagens;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private CheckBox cb_qtd, cb_valor;
    private EditText ed_valor_passagem, ed_qtd_passagem, ed_valor_total;
    private Button bt_calcular, bt_limpar;
    private float qtd_passagens, vl_passagen, vl_total;
    private LinearLayout ll_qtd, ll_valor;
    private int qtd_or_valor = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        instanciaViews();
        addMaskMoney();
        addListenerInButtons();
        addEventInCheckBoxs();
        llQtdVisible();
        cb_qtd.setChecked(true);

    }

    protected void instanciaViews() {
        ll_qtd = (LinearLayout) findViewById(R.id.ll_qtd);
        ll_valor = (LinearLayout) findViewById(R.id.ll_valor);
        cb_qtd = (CheckBox) findViewById(R.id.cb_qtd);
        cb_valor = (CheckBox) findViewById(R.id.cb_valor);
        ed_valor_passagem = (EditText) findViewById(R.id.ed_valor_passagem);
        ed_qtd_passagem = (EditText) findViewById(R.id.ed_qtd_passagem);
        ed_valor_total = (EditText) findViewById(R.id.ed_valor_total);
        bt_calcular = (Button) findViewById(R.id.bt_calcular);
        bt_limpar = (Button) findViewById(R.id.bt_limpar);
    }

    protected void addMaskMoney() {
        ed_valor_passagem.addTextChangedListener(new MonetaryMask(ed_valor_passagem) {
        });
        ed_valor_total.addTextChangedListener(new MonetaryMask(ed_valor_total) {
        });
    }

    protected void addListenerInButtons() {
        bt_calcular.setOnClickListener(clickInButtons);
        bt_limpar.setOnClickListener(clickInButtons);
    }

    protected void addEventInCheckBoxs() {
        cb_qtd.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (buttonView.isChecked()) {
                    cb_qtd.setChecked(isChecked);
                    cb_valor.setChecked(false);
                    llQtdVisible();
                    qtd_or_valor = 0;
                }
            }
        });

        cb_valor.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (buttonView.isChecked()) {
                    cb_valor.setChecked(isChecked);
                    cb_qtd.setChecked(false);
                    llValorVisible();
                    qtd_or_valor = 1;
                }
            }
        });
    }

    @SuppressLint("WrongConstant")
    protected void llQtdVisible() {
        ll_qtd.setVisibility(1);
        ll_valor.setVisibility(View.GONE);
    }

    @SuppressLint("WrongConstant")
    protected void llValorVisible() {
        ll_valor.setVisibility(1);
        ll_qtd.setVisibility(View.GONE);
    }

    protected void getDataQtd() {
        if (!ed_valor_passagem.getText().toString().isEmpty()) {
            if (!ed_qtd_passagem.getText().toString().isEmpty()) {
                vl_passagen = Float.parseFloat(ed_valor_passagem.getText().toString().replace("R$","").replace(",","."));
                qtd_passagens = Integer.parseInt(ed_qtd_passagem.getText().toString());

            }else{
                Toast.makeText(this, "informe a quantidade de passagens!", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(this, "informe o valor da passagem!", Toast.LENGTH_SHORT).show();
        }
    }

    protected void getDataValor() {
        if (!ed_valor_passagem.getText().toString().isEmpty()) {
            if (!ed_valor_total.getText().toString().isEmpty()) {
                vl_passagen = Float.parseFloat(ed_valor_passagem.getText().toString().replace("R$","").replace(",","."));
                vl_total = Float.parseFloat(ed_valor_total.getText().toString().replace("R$","").replace(",","."));
            }else{
                Toast.makeText(this, "informe o valor que deseja saber a quantidade de passagens!", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(this, "informe o valor da passagem!", Toast.LENGTH_SHORT).show();
        }
    }

    public void showAlertDialogErrorType(String msg) {

        String title = "Contador de passagens";
        new AlertDialog.Builder(this)
                .setTitle(title)
                .setMessage(msg)
                .setPositiveButton("OK", null)
                .show();
    }

    protected void limparCampos() {
        ed_valor_passagem.setText("");
        ed_qtd_passagem.setText("");
        ed_valor_total.setText("");
    }

    View.OnClickListener clickInButtons = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case (R.id.bt_calcular): {
                    String res = null;
                    if(qtd_or_valor == 0) {
                        getDataQtd();
                        res = "Para colocar " + ed_qtd_passagem.getText().toString() + " passagens é necessário: R$" + String.valueOf(qtd_passagens * vl_passagen).replace(".", ",");
                    }else{
                        getDataValor();
                        res = "Com" + ed_valor_total.getText().toString() + " é possivél colocar: " +vl_total/vl_passagen + "com um troco de " +vl_total%vl_passagen;
                    }
                    showAlertDialogErrorType(res);
                }
                case (R.id.bt_limpar): {

                }
            }
        }
    };
}
